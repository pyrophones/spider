function Camera(fov, aspect, zNear, zFar)
{
	this.fov = fov; //Field of view
	this.aspect = aspect; //Aspect ratio
	this.zNear = zNear; //Near clipping plane
	this.zFar = zFar; //Far clipping plane

	this.transMat = mat4.create(); //Transformation matrix
	this.viewMat = mat4.create(); //View matrix
	this.projMat = mat4.create(); //Projection matrix

	this.position = vec3.fromValues(0.0, 0.0, 0.0); //Position of the camera
	this.scale = vec3.fromValues(1.0, 1.0, 1.0); //Scale of the camera
	this.rotation = quat.create(); //Rotation of the camera

	this.front = vec3.create(); //Front vector
	this.right = vec3.create(); //Right vector
	this.up = vec3.create(); //Up vector
	this.speed = 10; //The speed of the camera

	this.update = function() {
		mat4.fromRotationTranslationScale(this.transMat, this.rotation, this.position, this.scale); //Set transformation matrix

		let fixTrans = mat3.create();
		mat3.fromMat4(fixTrans, this.transMat); //Strip matrix of translation component
		vec3.transformMat3(this.front, vec3.fromValues(0.0, 0.0, -1.0), fixTrans); //Set front vector

		//Set right vector
		vec3.transformMat3(this.up, vec3.fromValues(0.0, -1.0, 0.0), fixTrans);
		vec3.cross(this.right, this.front, this.up)
		vec3.normalize(this.right, this.right);

		//Set up vector
		vec3.cross(this.up, this.front, this.right);
		vec3.normalize(this.up, this.up);

		let forward = vec3.create();
		vec3.add(forward, this.position, this.front);

		mat4.lookAt(this.viewMat, this.position, forward, this.up); //Set view matrix
		mat4.perspective(this.projMat, this.fov, this.aspect, this.zNear, this.zFar); //Set projection matrix
	}
}
