function Texture(gl, url, isSkybox)
{
	this.texture = 0; //The texture location
	this.image = new Image(); //The image that will be loaded
	this.type = 0; //The gl image type

	//Loads a texture
	this.loadTexture = function(gl, url) {
		this.texture = gl.createTexture(); //Create a texture
		this.type = gl.TEXTURE_2D; //Set the texture type to a 2d texture
		gl.bindTexture(this.type, this.texture); //Bind the texture

		//Set some default variables to make a one pixel black texture while waiting for the image to actually load
		const level = 0;
		const internalFormat = gl.RGBA;
		const width = 1;
		const height = 1;
		const border = 0;
		const srcFormat = gl.RGBA;
		const srcType = gl.UNSIGNED_BYTE;
		const pixel = new Uint8Array([0, 0, 0, 255]);

		//Actually sets the texture
		gl.texImage2D(this.type, level, internalFormat, width, height, border, srcFormat, srcType, pixel);

		//Set the image onload function
		this.image.onload = function() {
			//Bind the texture and set it to the newly loaded images information
			gl.bindTexture(this.type, this.texture);
			gl.texImage2D(this.type, level, internalFormat, srcFormat, srcType, this.image);

			//If the image is a power of two, set the wrapping mode to repeat
			if (isPowerOf2(this.image.width) && isPowerOf2(this.image.height)) {
				gl.texParameteri(this.type, gl.TEXTURE_WRAP_S, gl.REPEAT);
				gl.texParameteri(this.type, gl.TEXTURE_WRAP_T, gl.REPEAT);
				gl.generateMipmap(this.type); //Generate mipmaps
			}

			//If it's not a power of two, we have to set the wrapping move to clamp to edge
			else {
				gl.texParameteri(this.type, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
				gl.texParameteri(this.type, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
			}

			//Use bilinear filtering for both minification and magnification
			gl.texParameteri(this.type, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
			gl.texParameteri(this.type, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		};

		this.image.onload = this.image.onload.bind(this); //Bind this so the onload works properly

		this.image.src = url; //Set image source to load it
	};

	//Loads a skybox. Skyboxes are special since they are 6 textures treated as one
	this.loadSkybox = function(gl, url) {
		this.texture = gl.createTexture(); //Create a texture
		this.type = gl.TEXTURE_CUBE_MAP; //Set the type to a cube map
		gl.bindTexture(this.type, this.texture); //Bind the texture

		//Still make a single black pixel for the texture while the other images load
		const level = 0;
		const internalFormat = gl.RGBA;
		const width = 1;
		const height = 1;
		const border = 0;
		const srcFormat = gl.RGBA;
		const srcType = gl.UNSIGNED_BYTE;
		const pixel = new Uint8Array([0, 0, 0, 255]);

		//Loop for the 6 textures
		for(let i = 0; i < 6; i++)
			gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, level, internalFormat, width, height, border, srcFormat, srcType, pixel);

		this.image = []; //Make images into an array

		//Loop for the images to set their callbacks and load them
		for(let i = 0; i < 6; i++) {
			this.image.push(new Image()); //Add a new image to the array
			this.image[i].onload = function() { //Set the image onload callback
				gl.bindTexture(this.type, this.texture); //Bind the texture
				gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, level, internalFormat, srcFormat, srcType, this.image[i]); //Set the cubemap to the image data

				//Cubemap wrapping should always be clamp to edge, so it doesn't matter if it's a power of two or not
				gl.texParameteri(this.type, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
				gl.texParameteri(this.type, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
				gl.texParameteri(this.type, gl.TEXTURE_WRAP_R, gl.CLAMP_TO_EDGE);
				//Still use bilinear filtering for minification and magnification
				gl.texParameteri(this.type, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
				gl.texParameteri(this.type, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
			};

			this.image[i].onload = this.image[i].onload.bind(this); //Bind this for the onload
			this.image[i].src = url + (i + 1) + ".png"; //Set the image name to load
		}
	};

	//Binds the texture for the draw
	this.bind = function() {
		gl.bindTexture(this.type, this.texture);
	};

	//Ternary operator for if it's a texture or a skybox
	(isSkybox ? this.loadSkybox(gl, url) : this.loadTexture(gl, url));
}

//Helper function that checks if a number is a power of two
function isPowerOf2(value)
{
	return (value & (value - 1)) == 0;
}
