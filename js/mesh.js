function Mesh(gl, shader, verts)
{
	this.verts = new Float32Array(verts); //The vertices of the mesh
	this.shader = shader; //The shader the mesh uses
	this.vbo = 0; //The VBO for binding to the graphics card
	this.transMat = mat4.create(); //The transformation matrix of the mesh

	this.position = vec3.fromValues(0.0, 0.0, 0.0); //The position of the mesh
	this.scale = vec3.fromValues(1.0, 1.0, 1.0); //The scale of the mesh
	this.rotation = quat.create(); //The rotation of the mesh

	this.front = vec3.fromValues(0.0, 0.0, -1.0); //The front vector of the mesh
	this.right = vec3.fromValues(1.0, 0.0, 0.0); //The right vector of the mesh
	this.up = vec3.fromValues(0.0, 1.0, 0.0); //The up vector of the mesh

	this.texture = 0; //The texture of the mesh

	//Tells the graphics card what data to read and how to read it
	this.buffer = function(gl) {
		//Create the VBO and assign its data
		this.vbo = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
		gl.bufferData(gl.ARRAY_BUFFER, this.verts, gl.STATIC_DRAW);

		//Set the vertex attribute of the shader if it exists
		if(shader.attribLocations.vertex != -1) {
			gl.vertexAttribPointer(shader.attribLocations.vertex, 3, gl.FLOAT, gl.FALSE, 4 * 5, 0);
			gl.enableVertexAttribArray(shader.attribLocations.vertex);
		}

		//Set the uv attribute of the shader
		if(shader.attribLocations.uv != -1) {
			gl.vertexAttribPointer(shader.attribLocations.uv, 2, gl.FLOAT, gl.FALSE, 4 * 5, 4 * 3);
			gl.enableVertexAttribArray(shader.attribLocations.uv);
		}
	};

	//Updates the transformation matrix of the mesh. Called once every frame
	this.update = function() {
		//Perform a matrix transformation
		mat4.fromRotationTranslationScale(this.transMat, this.rotation, this.position, this.scale);
		let fixTrans = mat3.create();
		mat3.fromMat4(fixTrans, this.transMat); //Strip matrix of translation
		vec3.transformMat3(this.front, vec3.fromValues(0.0, 0.0, -1.0), fixTrans); //Set front vector

		//Set right vector
		vec3.transformMat3(this.up, vec3.fromValues(0.0, -1.0, 0.0), fixTrans);
		vec3.cross(this.right, this.front, this.up)
		vec3.normalize(this.right, this.right);

		//Set up vector
		vec3.cross(this.up, this.front, this.right);
		vec3.normalize(this.up, this.up);
	};

	//Draws the mesh. Called once every frame
	this.draw = function(gl, cam) {
		this.shader.use(gl); //Set the current shader

		//Bind the active texture to the shader
		gl.activeTexture(gl.TEXTURE0);
		gl.uniform1i(this.shader.uniformLocations.albedo, 0);
		this.texture.bind();

		//Pass data to the uniform locations if they exist
		if(this.shader.uniformLocations.world != -1)
			gl.uniformMatrix4fv(this.shader.uniformLocations.world, false, this.transMat);
		if(this.shader.uniformLocations.proj != -1)
			gl.uniformMatrix4fv(this.shader.uniformLocations.proj, false, cam.projMat);
		if(this.shader.uniformLocations.view != -1)
		gl.uniformMatrix4fv(this.shader.uniformLocations.view, false, cam.viewMat);

		//Bind the buffer and draw
		gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
		gl.drawArrays(gl.TRIANGLES, 0, 36);
	};

	this.buffer(gl);
}
