"use strict"

window.onload = init;

//Vertices for a cube
const CUBE_VERTICES = [
	-0.5, -0.5, -0.5,  0.0, 0.0,
	 0.5, -0.5, -0.5,  1.0, 0.0,
	 0.5,  0.5, -0.5,  1.0, 1.0,
	 0.5,  0.5, -0.5,  1.0, 1.0,
	-0.5,  0.5, -0.5,  0.0, 1.0,
	-0.5, -0.5, -0.5,  0.0, 0.0,

	-0.5, -0.5,  0.5,  0.0, 0.0,
	 0.5, -0.5,  0.5,  1.0, 0.0,
	 0.5,  0.5,  0.5,  1.0, 1.0,
	 0.5,  0.5,  0.5,  1.0, 1.0,
	-0.5,  0.5,  0.5,  0.0, 1.0,
	-0.5, -0.5,  0.5,  0.0, 0.0,

	-0.5,  0.5,  0.5,  1.0, 0.0,
	-0.5,  0.5, -0.5,  1.0, 1.0,
	-0.5, -0.5, -0.5,  0.0, 1.0,
	-0.5, -0.5, -0.5,  0.0, 1.0,
	-0.5, -0.5,  0.5,  0.0, 0.0,
	-0.5,  0.5,  0.5,  1.0, 0.0,

	 0.5,  0.5,  0.5,  1.0, 0.0,
	 0.5,  0.5, -0.5,  1.0, 1.0,
	 0.5, -0.5, -0.5,  0.0, 1.0,
	 0.5, -0.5, -0.5,  0.0, 1.0,
	 0.5, -0.5,  0.5,  0.0, 0.0,
	 0.5,  0.5,  0.5,  1.0, 0.0,

	-0.5, -0.5, -0.5,  0.0, 1.0,
	 0.5, -0.5, -0.5,  1.0, 1.0,
	 0.5, -0.5,  0.5,  1.0, 0.0,
	 0.5, -0.5,  0.5,  1.0, 0.0,
	-0.5, -0.5,  0.5,  0.0, 0.0,
	-0.5, -0.5, -0.5,  0.0, 1.0,

	-0.5,  0.5, -0.5,  0.0, 1.0,
	 0.5,  0.5, -0.5,  1.0, 1.0,
	 0.5,  0.5,  0.5,  1.0, 0.0,
	 0.5,  0.5,  0.5,  1.0, 0.0,
	-0.5,  0.5,  0.5,  0.0, 0.0,
	-0.5,  0.5, -0.5,  0.0, 1.0
];

//Initializes the engine
function init()
{
	//Get the canvas
	const canvas = document.getElementById("glCanvas");

	//Initialize the engine
	const engine = new Engine(canvas);

	//Add event listeners for key up and key down
	document.addEventListener('keyup', function(event) {
		engine.input.keyUpEvent(event);
	});

	document.addEventListener('keydown', function(event) {
		engine.input.keyDownEvent(event);
	});

	//Load the shaders
	if(!engine.createShader(document.getElementById("vertShader").innerHTML, document.getElementById("fragShader").innerHTML, "defShader")) {
		return;
	}

	if(!engine.createShader(document.getElementById("skyboxVertShader").innerHTML, document.getElementById("skyboxFragShader").innerHTML, "skyboxShader")) {
		return;
	}

	//Initialize some things
	engine.createMesh(engine.shaders.defShader, CUBE_VERTICES, "cube");
	engine.createCamera(45 * Math.PI / 180.0, engine.gl.canvas.clientWidth / engine.gl.canvas.clientHeight, 0.01, 1000.0);
	engine.loadTexture("media/planks.png", "planks", false);
	engine.loadTexture("media/interstellar/", "interstellar", true);
	engine.meshes["cube"].texture = engine.textures["planks"];
	engine.createSkybox(engine.shaders.skyboxShader, engine.textures["interstellar"]);

	//Run the engine
	engine.run();
}

function Engine(canvas)
{
	//Start a webgl context
	this.gl = canvas.getContext("webgl2");
	let isWebGL2 = !!this.gl;
	if(!isWebGL2) {
		 console.log("Your browser does not support webgl2!")
		 this.gl = canvas.getContext("webgl");
	}

	//Only continue if WebGL is available and working
	if (!this.gl) {
		 console.log("Your browser does not support webgl!")
		 return;
	}

	this.gl.clearColor(0.0, 0.0, 0.0, 1.0); //Set the clear color to black

	this.skybox = 0; //The skybox
	this.meshes = {}; //The loaded meshes
	this.shaders = {}; //The loaded shaders
	this.textures = {}; //The loaded textures
	this.cam = 0; //The camera
	this.prevTime = 0; //The previous time
	this.curTime = 0; //The current time
	this.deltaTime = 0; //The difference in time
	this.input = new Input(); //The input manager

	//Creates a shader
	this.createShader = function(vsSource, fsSource, name) {
		let shader = new Shader(this.gl, vsSource, fsSource);

		if(shader.program != -1) {
			this.shaders[name] = shader;
			return 1;
		}

		else {
			console.log("Failed to load shader!");
			return 0;
		}
	};

	//Creates a mesh
	this.createMesh = function(shader, verts, name) {
		let mesh = new Mesh(this.gl, shader, verts);
		this.meshes[name] = mesh;
	};

	//Creates a skybox
	this.createSkybox = function(shader, texture) {
		this.skybox = new Mesh(this.gl, shader, CUBE_VERTICES);
		this.skybox.texture = texture;
	}

	//Loads a texture
	this.loadTexture = function(url, name, isSkybox) {
		let texture = new Texture(this.gl, url, isSkybox);
		this.textures[name] = texture;
	}

	//Creates a camera
	this.createCamera = function(fov, aspect, zNear, zFar) {
		this.cam = new Camera(fov, aspect, zNear, zFar);
	};

	//Runs the game
	this.run = function() {
		this.update();
		this.draw();

		//This is used to do animation and updating of things
		window.requestAnimationFrame(this.run.bind(this));
	};

	//Updates the scene
	this.update = function() {
		this.curTime += 0.001;
		this.deltaTime = this.prevTime - this.curTime;
		this.prevTime = this.curTime;

		this.meshes["cube"].position = vec3.fromValues(0.0, 0.0, -5.0);
		quat.setAxisAngle(this.meshes["cube"].rotation, vec3.fromValues(0.0, 1.0, 0.0),this.curTime);
		this.meshes["cube"].scale = vec3.fromValues(1.0, 1.0, 1.0);

		for(var mesh in this.meshes) {
			this.meshes[mesh].update();
		}

		this.cam.update();

		let movement = vec3.create();
		let scaled = vec3.create();

		//Forward movement
		if(this.input.getKey("w") || this.input.getKey("W")) {
			vec3.scale(scaled, this.cam.front, -this.deltaTime * this.cam.speed);
			vec3.add(movement, scaled, movement)
		}

		//Backward movement
		if(this.input.getKey("s") || this.input.getKey("S")) {
			vec3.scale(scaled, this.cam.front, this.deltaTime * this.cam.speed);
			vec3.add(movement, scaled, movement)
		}

		//Left movement
		if(this.input.getKey("a") || this.input.getKey("A")) {
			vec3.scale(scaled, this.cam.right, -this.deltaTime * this.cam.speed);
			vec3.add(movement, scaled, movement)
		}

		//Right movement
		if(this.input.getKey("d") || this.input.getKey("D")) {
			vec3.scale(scaled, this.cam.right, this.deltaTime * this.cam.speed);
			vec3.add(movement, scaled, movement)
		}

		vec3.add(this.cam.position, this.cam.position, movement);
	};

	//Draws the scene
	this.draw = function() {
		this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT); //Clear the backbuffer
		this.gl.enable(this.gl.DEPTH_TEST); //Enable depth testing
		this.gl.depthFunc(this.gl.LESS); //Set the depth function to less than
		this.gl.cullFace(this.gl.BACK); //Cull back faces

		//Draw the meshes
		for(var mesh in this.meshes) {
			this.meshes[mesh].draw(this.gl, this.cam);
		}

		this.gl.disable(this.gl.BLEND); //Disable blending
		this.gl.depthFunc(this.gl.LEQUAL); //Set the depth function to less than or equal to
		this.gl.cullFace(this.gl.FRONT); //Cull front faces

		//Draw the skybox if there is one
		if(this.skybox != 0)
			this.skybox.draw(this.gl, this.cam);
	};
}
