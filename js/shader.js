function Shader(gl, vsSource, fsSource)
{
	this.program = 0; //The program reference num

	//Create a shader program
	this.createShaderProgram = function(gl, vsSource, fsSource) {
		const vertexShader = gl.createShader(gl.VERTEX_SHADER); //Create the vertex shader
		gl.shaderSource(vertexShader, vsSource); //Load the source
		gl.compileShader(vertexShader); //Compile it

		//If it doesn't load, print the error and return
		if(!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
			console.log('Vertex shader error: ' + gl.getShaderInfoLog(vertexShader));
			gl.deleteShader(vertexShader);
			gl.program = -1;
			return;
		}

		const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER); //Create the frag shader
		gl.shaderSource(fragmentShader, fsSource); //Load the source
		gl.compileShader(fragmentShader); //Compile it

		//If it doesn't load, print the error and return
		if(!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
			console.log('Fragment shader error: ' + gl.getShaderInfoLog(fragmentShader));
			gl.deleteShader(fragmentShader);
			gl.program = -1;
			return;
		}

		this.program = gl.createProgram(); //Create the shader program
		gl.attachShader(this.program, vertexShader); //Attach the vertex shader
		gl.attachShader(this.program, fragmentShader); //Attach the fragment shader
		gl.linkProgram(this.program); //Link the shader program

		//If it doesn't link, return
		if(!gl.getProgramParameter(this.program, gl.LINK_STATUS)) {
			console.log("Failed to link shader program!");
			gl.program = -1;
			return;
		}

		//Try to get the attribues for vertex and uv
		this.attribLocations["vertex"] = gl.getAttribLocation(this.program, "vertex");
		this.attribLocations["uv"] = gl.getAttribLocation(this.program, "uv");

		//Try to get the uniforms for the world, projection, and view matrices and the sampler
		this.uniformLocations["world"] = gl.getUniformLocation(this.program, "world");
		this.uniformLocations["proj"] = gl.getUniformLocation(this.program, "proj");
		this.uniformLocations["view"] = gl.getUniformLocation(this.program, "view");
		this.uniformLocations["albedo"] = gl.getUniformLocation(this.program, "albedo");
	};

	//Uses the shader
	this.use = function(gl) {
		gl.useProgram(this.program);
	};

	this.attribLocations = {}; //List of shader attributes
	this.uniformLocations = {}; //List of shader uniforms

	this.createShaderProgram(gl, vsSource, fsSource);
}
