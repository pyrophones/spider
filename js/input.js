function Input()
{
	this.keyMap = {}; //Map of the keyts

	//Checks if the specified key is active
	this.getKey = function(key) {
		return this.keyMap[key];
	}

	//Key down callback
	this.keyDownEvent = function(event) {
		this.keyMap[event.key] = true;
	}

	//Key up callback
	this.keyUpEvent = function(event) {
		this.keyMap[event.key] = false;
	}
}
